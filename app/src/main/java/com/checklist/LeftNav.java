package com.checklist;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import utilis.CheckListPref;
import utilis.Constants;
import utilis.IOnFragmentInteraction;


@SuppressWarnings("deprecation")
public class LeftNav extends Fragment implements OnClickListener {

    private NavigationDrawerCallbacks mCallbacks;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;
    private TextView tvOpt1, tvOpt2, tvOpt3, tvOpt4, tvOpt5, tvOpt6, tvOpt7, tvOpt8;
    private ImageView ivOpt1, ivOpt2, ivOpt3, ivOpt4, ivOpt5, ivOpt6, ivOpt7, ivOpt8;
    private int currentPos = 0;

    private String url = null, menuKey = null;
    private static final String DEF_POS = "defpos";
    public static final String URL_KEY = "urlKey";
    public static final String MENU_KEY = "menuKey";
    public static final String MENU_1 = "HOME";
    private static final String MENU_2 = "ABOUT";
    private static final String MENU_3 = "PROJECTS";
    private static final String MENU_4 = "RESALE";
    private static final String MENU_5 = "RENT";
    private static final String MENU_6 = "EXCHANGE PROPERTY";
    private static final String MENU_7 = "POST PROPERTY";
    private static final String MENU_8 = "CONTACT US";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_nav, container, false);
        initializeView(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        if (savedInstanceState != null) {
            currentPos = savedInstanceState.getInt(DEF_POS);
            System.out.println("currentPos " + currentPos);
        }
    }

    private void initializeView(View view) {

        view.findViewById(R.id.linearLayout_1).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_2).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_3).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_4).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_5).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_6).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_7).setOnClickListener(this);
        view.findViewById(R.id.linearLayout_8).setOnClickListener(this);

        ivOpt1 = (ImageView) view.findViewById(R.id.ln_iv_opt_1);
        ivOpt2 = (ImageView) view.findViewById(R.id.ln_iv_opt_2);
        ivOpt3 = (ImageView) view.findViewById(R.id.ln_iv_opt_3);
        ivOpt4 = (ImageView) view.findViewById(R.id.ln_iv_opt_4);
        ivOpt5 = (ImageView) view.findViewById(R.id.ln_iv_opt_5);
        ivOpt6 = (ImageView) view.findViewById(R.id.ln_iv_opt_6);
        ivOpt7 = (ImageView) view.findViewById(R.id.ln_iv_opt_7);
        ivOpt8 = (ImageView) view.findViewById(R.id.ln_iv_opt_8);

        tvOpt1 = (TextView) view.findViewById(R.id.ln_tv_opt_1);
        tvOpt2 = (TextView) view.findViewById(R.id.ln_tv_opt_2);
        tvOpt3 = (TextView) view.findViewById(R.id.ln_tv_opt_3);
        tvOpt4 = (TextView) view.findViewById(R.id.ln_tv_opt_4);
        tvOpt5 = (TextView) view.findViewById(R.id.ln_tv_opt_5);
        tvOpt6 = (TextView) view.findViewById(R.id.ln_tv_opt_6);
        tvOpt7 = (TextView) view.findViewById(R.id.ln_tv_opt_7);
        tvOpt8 = (TextView) view.findViewById(R.id.ln_tv_opt_8);

        setBackgroundMenu(currentPos);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(DEF_POS, currentPos);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View view) {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }

        switch (view.getId()) {
            case R.id.linearLayout_1:
                currentPos = 0;
                setBackgroundMenu(currentPos);
                url = Constants.URL + Constants.URL_LIST;
                menuKey = MENU_1;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_2:
                currentPos = 1;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "about/" + Constants.URL_LIST;
                menuKey = MENU_2;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_3:
                currentPos = 2;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "projects/" + Constants.URL_LIST;
                menuKey = MENU_3;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_4:
                currentPos = 3;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "resale/" + Constants.URL_LIST;
                menuKey = MENU_4;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_5:
                currentPos = 4;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "rent/" + Constants.URL_LIST;
                menuKey = MENU_5;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_6:
                currentPos = 5;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "exchange-property/" + Constants.URL_LIST;
                menuKey = MENU_6;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_7:
                currentPos = 6;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "post-property/" + Constants.URL_LIST;
                menuKey = MENU_7;
                initiateFragment(url, menuKey);
                break;

            case R.id.linearLayout_8:
                currentPos = 7;
                setBackgroundMenu(currentPos);
                url = Constants.URL + "contact-us/" + Constants.URL_LIST;
                menuKey = MENU_8;
                initiateFragment(url, menuKey);
                break;

            default:
                break;
        }
    }

    public boolean isDrawerOpen() {

        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        if (mDrawerLayout != null)
            mDrawerLayout.closeDrawer(Gravity.LEFT);

       // mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.LEFT);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);


        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                if (!isAdded()) {
                    return;
                }
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null && fragment instanceof IOnFragmentInteraction) {
                    ((IOnFragmentInteraction) fragment).onDrawerOpenOrClose(false);
                }
                getActivity().supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                if (!isAdded()) {
                    return;
                }
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
                if (fragment != null && fragment instanceof IOnFragmentInteraction) {

                    ((IOnFragmentInteraction) fragment).onDrawerOpenOrClose(true);
                }
                getActivity().supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.openDrawer(mFragmentContainerView);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    @SuppressWarnings("unused")
    private void selectItem(Fragment fragment) {
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(fragment);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(Fragment fragment);
    }

    /**
     * @param position
     */

    private void setBackgroundMenu(int position) {

        int color = R.color.black;
        int selectedColor = R.color.red;

        ivOpt1.setBackgroundResource(R.drawable.left_nav_1);
        ivOpt2.setBackgroundResource(R.drawable.left_nav_2);
        ivOpt3.setBackgroundResource(R.drawable.left_nav_3);
        ivOpt4.setBackgroundResource(R.drawable.left_nav_4);
        ivOpt5.setBackgroundResource(R.drawable.left_nav_5);
        ivOpt6.setBackgroundResource(R.drawable.left_nav_6);
        ivOpt7.setBackgroundResource(R.drawable.left_nav_7);
        ivOpt8.setBackgroundResource(R.drawable.left_nav_6);

        tvOpt1.setText(MENU_1);
        tvOpt2.setText(MENU_2);
        tvOpt3.setText(MENU_3);
        tvOpt4.setText(MENU_4);
        tvOpt5.setText(MENU_5);
        tvOpt6.setText(MENU_6);
        tvOpt7.setText(MENU_7);
        tvOpt8.setText(MENU_8);

        tvOpt1.setTextColor(getResources().getColor(color));
        tvOpt2.setTextColor(getResources().getColor(color));
        tvOpt3.setTextColor(getResources().getColor(color));
        tvOpt4.setTextColor(getResources().getColor(color));
        tvOpt5.setTextColor(getResources().getColor(color));
        tvOpt6.setTextColor(getResources().getColor(color));
        tvOpt7.setTextColor(getResources().getColor(color));
        tvOpt8.setTextColor(getResources().getColor(color));

        switch (position) {
            case 0:
                tvOpt1.setTextColor(getResources().getColor(selectedColor));
               // ivOpt1.setBackgroundResource(R.drawable.left_nav_1_selector);
                ivOpt1.setBackgroundResource(R.drawable.i1);
                break;
            case 1:
                tvOpt2.setTextColor(getResources().getColor(selectedColor));
                //ivOpt2.setBackgroundResource(R.drawable.left_nav_2_selector);
                ivOpt2.setBackgroundResource(R.drawable.i22);
                break;
            case 2:
                tvOpt3.setTextColor(getResources().getColor(selectedColor));
               // ivOpt3.setBackgroundResource(R.drawable.left_nav_3_selector);
                ivOpt3.setBackgroundResource(R.drawable.i33);

                break;
            case 3:
                tvOpt4.setTextColor(getResources().getColor(selectedColor));
               // ivOpt4.setBackgroundResource(R.drawable.left_nav_4_selector);
                ivOpt4.setBackgroundResource(R.drawable.i44);

                break;
            case 4:
                tvOpt5.setTextColor(getResources().getColor(selectedColor));
               // ivOpt5.setBackgroundResource(R.drawable.left_nav_5_selector);
                ivOpt5.setBackgroundResource(R.drawable.i55);

                break;
            case 5:
                tvOpt6.setTextColor(getResources().getColor(selectedColor));
                //ivOpt6.setBackgroundResource(R.drawable.left_nav_6_selector);
                ivOpt6.setBackgroundResource(R.drawable.i66);


                break;
            case 6:
                tvOpt7.setTextColor(getResources().getColor(selectedColor));
                //ivOpt7.setBackgroundResource(R.drawable.left_nav_7_selector);
                ivOpt7.setBackgroundResource(R.drawable.i77);

                break;
            case 7:
                tvOpt8.setTextColor(getResources().getColor(selectedColor));
                //ivOpt8.setBackgroundResource(R.drawable.left_nav_6_selector);
                ivOpt8.setBackgroundResource(R.drawable.i66);

                break;

        }

    }


    /**
     * initiate fragment for web views
     */
    private void initiateFragment(String url, String menuKey) {
        Fragment frag = new WebViewFragments();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString(URL_KEY, url);
        bundle.putString(MENU_KEY, menuKey);
        frag.setArguments(bundle);
        ft.replace(R.id.container, frag);
        ft.commit();
    }

}