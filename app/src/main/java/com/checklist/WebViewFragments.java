package com.checklist;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import utilis.CheckListPref;
import utilis.Constants;
import utilis.IResponse;
import utilis.JSONParser;


/**
 * Created by brst-pc79 on 2/25/16.
 */
public class WebViewFragments extends Fragment {
    private WebView mWebView;
    private String loadUrl, menuKey;
    private IResponse postResponse;
    private ProgressDialog dialog;

    Bundle bundle;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_view_load, container, false);
        mWebView = (WebView) view.findViewById(R.id.webView);
        ((ActionBarActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(getResources().getColor(R.color.red)));
        dialog=new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        //dialog.setCancelable(false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            loadUrl = bundle.getString(LeftNav.URL_KEY);
            menuKey = bundle.getString(LeftNav.MENU_KEY);
        } else {
            loadUrl = Constants.URL + Constants.URL_LIST;
            menuKey = LeftNav.MENU_1;
        }
        ((TextView) getActivity().findViewById(R.id.tv_title)).setText(menuKey);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //UI.showProgressDialog(getActivity());
                dialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //pd.dismiss();
                //UI.hideProgressDialog();
                dialog.dismiss();
                String webUrl = mWebView.getUrl();
            }
        });
        Log.e("urll",loadUrl);
        mWebView.loadUrl(loadUrl);

    }
}
