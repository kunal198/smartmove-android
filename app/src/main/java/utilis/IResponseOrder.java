package utilis;

public interface IResponseOrder {
	public void onAddresses(String response);
	public void onTimeSlotsListener(String response);
	public void onCategoriesListener(String response);
	public void onAreaListener(String response);
	public void onLastFiveDelivery(String response);

}
