package utilis;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class CheckListPref {

	public static final String GET_STARTED = "getstarted";

	private static CheckListPref mInstance;
	private SharedPreferences mPref, defaultPref;
	private static String newPref = "new_pref";
	private String started;
	public  String getStarted() {
		return started;
	}

	public void saveStarted(String getStarted) {
		this.started = getStarted;
		mPref.edit().putString(GET_STARTED,getStarted).commit();
	}

	private CheckListPref(Context context) {
		mPref = PreferenceManager.getDefaultSharedPreferences(context);
		reloadPrefrence();
	}
	public static SharedPreferences getMpref(Context context) {
		return context.getSharedPreferences(newPref, 0);
	}
	private void reloadPrefrence() {
		started=mPref.getString(GET_STARTED,null);
	}
	public static CheckListPref getInstance(Context context) {
		return mInstance == null ? (mInstance = new CheckListPref(context)) : mInstance;
	}
	public void clearPref() {
		mPref.edit().clear().commit();
		reloadPrefrence();
	}
}
