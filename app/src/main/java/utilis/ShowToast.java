package utilis;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Ankur on 4/6/2015.
 */
public class ShowToast {
	public static void displayToast(Context context, String message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
