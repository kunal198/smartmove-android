package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import utilis.ConnectionDetector;
import utilis.IResponse;
import utilis.ShowToast;

public class PostServerResponse extends AsyncTask<Void, Void, String> {
	private static final String NETWORK_ISSUE = "NetworkIssue";
	private Context mContext;
	private IResponse onReceiver;
	private String url;
	private ProgressDialog mDialog;
	private JSONObject userData;

	public PostServerResponse(Context context, IResponse responseListener, JSONObject jobj, String urlPost) {
		mContext = context;
		onReceiver = responseListener;
		userData = jobj;
		url = urlPost;
	}

	@Override
	protected void onPreExecute() {
		try {
			if (mDialog == null)
				mDialog = new ProgressDialog(mContext);
			mDialog.setMessage("Loading...");
			mDialog.setCancelable(false);
			mDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected String doInBackground(Void... params) {
		if (!new ConnectionDetector(mContext).isConnectedToInternet())
			return NETWORK_ISSUE;

		String line = "";
		String result = "";

		HttpClient client = new DefaultHttpClient();
		try {
			HttpPost post = new HttpPost(url);
			StringEntity se = new StringEntity(userData.toString());
			post.setHeader("Content-type", "application/json");
			post.setHeader("Accept", "application/json");
			/*post.setHeader("X-User-Mobile", QuickMilkPref.getInstance(mContext).getMobileNumber());
			post.setHeader("X-User-Token", QuickMilkPref.getInstance(mContext).getUserToken());*/
			post.setEntity(se);

			HttpResponse response = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			while ((line = rd.readLine()) != null) {
				result += line;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.i("GetServerResponse", result);
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		try {
			if (mDialog != null)
				if (mDialog.isShowing())
					mDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (onReceiver != null) {
			if (result.equals(NETWORK_ISSUE))
				ShowToast.displayToast(mContext, "No internet connection, Please connect to internet first");
			else
				onReceiver.onReceiveResponse(result);
		}

	}
}
