package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import utilis.CheckListPref;
import utilis.IResponseOrder;
import utilis.ShowToast;


public class GetRequestApiResponse extends AsyncTask<Void, Void, String> {
	private static final String NETWORK_ISSUE = "NetworkIssue";
	private static final String CACHE = "cache";
	private static final String TAG = GetRequestApiResponse.class.getSimpleName();

	private String mUrl;
	private Context mContext;
	private IResponseOrder mIResoponse;
	private ProgressDialog mDialog;
	private CheckListPref checkListPref;

	public GetRequestApiResponse(Context context, String url, IResponseOrder iResponse) {
		mContext = context;
		mUrl = url;
		mIResoponse = iResponse;
		checkListPref = CheckListPref.getInstance(mContext);
	}

	@Override
	protected void onPreExecute() {
		try {
			if (mDialog == null && mIResoponse != null) {
				mDialog = new ProgressDialog(mContext);
				mDialog.setMessage("Please wait...");
				mDialog.setCancelable(false);
				mDialog.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected String doInBackground(Void... params) {

		Log.d(TAG, mUrl);
/*
		if (mIResoponse != null) {
			if (mUrl.contains("addresses") && milkPref.getAddresses() != null) {
				return CACHE;

			} else if (mUrl.contains("time_slots") && milkPref.getTimeSlot() != null) {
				return CACHE;

			} else if (mUrl.contains("categories") && milkPref.getCategory() != null) {
				return CACHE;

			} else if (mUrl.contains("states") && milkPref.getstates() != null) {
				return CACHE;
			} else if (mUrl.contains("customer_recent_delivered_deliveries") && milkPref.getDelivery() != null) {
				return CACHE;
			} else if (!new ConnectionDetector(mContext).isConnectedToInternet())
				return NETWORK_ISSUE;
		}*/

		String result = null;
		String line = "";
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(mUrl);

			//get.setHeader("Content-type", "application/json");
			//get.setHeader("Accept", "application/json");

			//Log.e("mobile", "j" + CheckListPref.getInstance(mContext));
			//Log.e("token","j"+CheckListPref.getInstance(mContext).getUserToken());

			//get.setHeader("X-User-Mobile", CheckListPref.getInstance(mContext).getMobileNumber());
			//get.setHeader("X-User-Token", CheckListPref.getInstance(mContext).getUserToken());

			HttpResponse response = client.execute(get);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			result = "";
			while ((line = rd.readLine()) != null) {
				result += line;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		try {
			if (mDialog != null)
				if (mDialog.isShowing())
					mDialog.dismiss();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Log.d(TAG, result);
			if (result.equals(CACHE) && mIResoponse != null) {
				if (mUrl.contains("login")) {
					mIResoponse.onAddresses(result);

				} /*else if (mUrl.contains("time_slots")) {
					mIResoponse.onTimeSlotsListener(checkListPref.getTimeSlot());

				} else if (mUrl.contains("categories")) {
					mIResoponse.onCategoriesListener(checkListPref.getCategory());

				} else if (mUrl.contains("states")) {
					mIResoponse.onAreaListener(checkListPref.getstates());
				}
				else if (mUrl.contains("customer_recent_delivered_deliveries")) {
					mIResoponse.onLastFiveDelivery(checkListPref.getDelivery());

				}*/
			} else if (result != null) {
				if (result.equals(NETWORK_ISSUE))
					ShowToast.displayToast(mContext, "No internet connection, Please connect to internet first");

				/*else if (mUrl.contains("login")) {
					checkListPref.saveAddresses(result);
					mIResoponse.onAddresses(result);

				} else if (mUrl.contains("time_slots")) {
					checkListPref.saveTimeSlot(result);
					mIResoponse.onTimeSlotsListener(result);

				} else if (mUrl.contains("categories")) {
					checkListPref.saveCategory(result);
					mIResoponse.onCategoriesListener(result);

				} else if (mUrl.contains("states")) {
					checkListPref.savestates(result);
					mIResoponse.onAreaListener(result);
					System.out.println("GetRequestApiResponse.onPostExecute() ");
				}
				else if (mUrl.contains("customer_recent_delivered_deliveries")) {
					checkListPref.setDelivery(result);
					mIResoponse.onLastFiveDelivery(result);
				}*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
