package adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.checklist.R;


/**
 * Created by brst-pc79 on 2/24/16.
 */
public class TutorialAdapter extends PagerAdapter {
    private FragmentActivity activity;
    FragmentManager manager;
    private int count;

    public TutorialAdapter(FragmentManager manager, FragmentActivity mActivity,
                           int count) {

        this.activity = mActivity;
        this.manager = manager;
        this.count = count;

    }

    /**
     * Determines whether a page View is associated with a specific key object
     * as returned by instantiateItem(ViewGroup, int).
     *
     * @param view   Page View to check for association with object
     * @param object Object to check for association with view
     * @return true if view is associated with the key object object.
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * Create the page for the given position.
     *
     * @param container The containing View in which the page will be shown.
     * @param position  The page position to be instantiated.
     * @return Returns an Object representing the new page. This does not need
     * to be a View, but can be some other container of the page.
     */
    @SuppressWarnings("static-access")
    @Override
    public Object instantiateItem(View container, int position) {

        ViewPager pager = (ViewPager) container;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(activity.LAYOUT_INFLATER_SERVICE);

        container = inflater.inflate(R.layout.pager_view, null);
        ImageView ivSliderImg = (ImageView) container
                .findViewById(R.id.iv_slider_img);
        if (position == 0)
            ivSliderImg.setImageResource(R.drawable.image1);
        else if (position == 1)
            ivSliderImg.setImageResource(R.drawable.image2);
        else if (position == 2)
            ivSliderImg.setImageResource(R.drawable.image3);
        System.out.println(" " + position);
        pager.addView(container);
        return container;
    }
    /**
     * setting postion non will update the startmove.brst.com.smartmove.adapter
     * if any modification done
     */
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    /**
     * Remove a page for the given position.
     *
     * @param container The containing View from which the page will be removed.
     * @param position  The page position to be removed.
     * @param view      The same object that was returned by instantiateItem(View,
     *                  int).
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        ((ViewPager) container).removeView((View) view);
    }

    @Override
    public int getCount() {
        return count;
    }
}
